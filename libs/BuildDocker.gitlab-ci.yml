.build-docker:
  stage: build
  image: docker:20.10.3

  script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
        docker info && break
        echo "Waiting for docker to start"
        sleep 1s
      done

    - |
      if [[ "$DEBUG" == "true" ]]; then
        set -x
      fi

      if [ -z "$PROJECT_ID" ]; then
        echo "Missing PROJECT_ID configuration"
        exit 1
      fi

      if [ -z "$GCP_KEY_B64" ] && [ -z "$GCP_KEY" ]; then
        echo "Missing Google credential configuration. Please set environment variable GCP_KEY"
        exit 1
      fi

      for i in $(seq 1 5); do
        if [ -n "$GCP_KEY_B64" ]; then
          echo "$GCP_KEY_B64" | base64 -d | docker login -u _json_key --password-stdin https://gcr.io && break
        else
          echo "$GCP_KEY" | docker login -u _json_key --password-stdin https://gcr.io && break
        fi

        echo "Waiting for docker to login"
        sleep 1s
      done

    - |
      [[ "$DEBUG" == "true" ]] && set -x

      [ -z "$IMAGE" ] && IMAGE=$CI_PROJECT_NAME
      [ -z "$IMAGE_LATEST_TAG" ] && IMAGE_LATEST_TAG=$(echo $CI_COMMIT_REF_NAME | tr "/" "_")-latest
      [ -z "$IMAGE_PRODUCTION_TAG" ] && IMAGE_PRODUCTION_TAG=prod-$CI_COMMIT_SHORT_SHA
      [ -z "$WORKFLOW" ] && WORKFLOW="GIT_FLOW"
      [ -z "$DOCKERFILE" ] && DOCKERFILE="Dockerfile"
      [ -z "$BUILD_LOCATION" ] && BUILD_LOCATION="."
      [ -z "$BUILD_CACHE" ] && BUILD_CACHE="true"

      if [ -z "$IMAGE_TAG" ]; then
        if [ -z "$CI_COMMIT_TAG" ]; then
          IMAGE_TAG=$(echo $CI_COMMIT_REF_NAME | tr "/" "_")-$CI_COMMIT_SHORT_SHA
        else
          if [[ "$DEPLOY_STRATEGY" == "job" ]]; then
            IMAGE_TAG=$CI_COMMIT_SHORT_SHA
            IMAGE_LATEST_TAG=$CI_COMMIT_SHORT_SHA-latest
          else
            IMAGE_TAG=$CI_COMMIT_TAG
            IMAGE_LATEST_TAG=$CI_COMMIT_TAG-latest
          fi
        fi
      fi

      if [ -n "$IMAGE_SUFFIX" ]; then
        IMAGE_NAME="$IMAGE-$IMAGE_SUFFIX"
      else
        IMAGE_NAME=$IMAGE
      fi

      set +e
      if [[ -n "$(echo $CI_COMMIT_REF_NAME | grep release/)" ]] && [[ -z "$(echo $BUILD_OPTIONS | grep always_rebuild)" ]]; then
        docker pull gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG}
        if [[ $? == "0" ]]; then
          echo "Skip building Docker image"
          if [ -e "artifacts/ci/version" ]; then
            tag=$(cat artifacts/ci/version)
          else
            tag="${CI_COMMIT_REF_NAME#release/}.0"
          fi
          docker tag gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG} gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${tag}
          docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${tag}
          exit 0
        fi
      fi
      set -e

      if [[ "$BUILD_CACHE" == "true" ]]; then
        echo "Using remote docker build cache"
        docker pull gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_LATEST_TAG} || true
        BUILD_CACHE_OPTION="--cache-from gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_LATEST_TAG}"
      fi

      if [ -z "$ENV" ]; then
        if [ -n "$CI_COMMIT_TAG" ]; then
          ENV="production"
        else
          ENV=$CI_COMMIT_REF_NAME
        fi
      fi

      echo "Building image $IMAGE_NAME:$IMAGE_TAG"

      DOCKER_BUILDKIT=1 docker build --pull \
        --build-arg GITLAB_TOKEN=$GITLAB_TOKEN \
        --build-arg ENV=$ENV $EXTRA_BUILD_ARGS \
        $BUILD_CACHE_OPTION \
        -f $DOCKERFILE \
        -t gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG} \
        -t gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_LATEST_TAG} \
        ${BUILD_LOCATION}
      docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG}

      if [[ "$BUILD_CACHE" == "true" ]]; then
        docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_LATEST_TAG}
      fi

      if [[ $WORKFLOW == "TRUNK_BASED" ]]; then
        echo "Publish image in trunk-based workflow"
        if [[ "$CI_COMMIT_REF_NAME" == "$CI_DEFAULT_BRANCH" ]]; then
          echo "Push image tag ${IMAGE_PRODUCTION_TAG}"
          docker tag gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG} gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_PRODUCTION_TAG}
          docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_PRODUCTION_TAG}
        fi

        if [ -e "artifacts/ci/version" ]; then
          echo "Push image tag $(cat artifacts/ci/version)"
          docker tag gcr.io/${PROJECT_ID}/${IMAGE_NAME}:${IMAGE_TAG} gcr.io/${PROJECT_ID}/${IMAGE_NAME}:$(cat artifacts/ci/version)
          docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}:$(cat artifacts/ci/version)
        fi

      fi



